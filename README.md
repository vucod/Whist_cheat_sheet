# Whist cheat sheet

Cheat sheet for the game "Whist à la couleur"

## Preview

<img src="preview.png"  height="600">

## Download

[PDF files in English, French and Dutch](https://gitlab.com/vucodil/Whist_cheat_sheet/tags)
